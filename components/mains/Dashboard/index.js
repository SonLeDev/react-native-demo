import React, {Component} from 'react'
import {View, Text, StyleSheet } from 'react-native'

export default class Dashboard extends Component {
	render(){
		return (
			      <View style={styles.base}>
			        <Text>{'You are the richest person in the world !!!'}</Text>
			      </View>
			    );
	}
}

const styles = StyleSheet.create({
  base: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})