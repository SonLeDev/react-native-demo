import { createBottomTabNavigator } from 'react-navigation'

import Dashboard from '../mains/Dashboard'
import Profile from '../mains/Profile'

const LoggedInNavigator = createBottomTabNavigator({
	Dashboard:{
		screen: Dashboard
	},
	Profile:{
		screen: Profile
	}
})

export default LoggedInNavigator
