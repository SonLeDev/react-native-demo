import { createStackNavigator } from 'react-navigation'

import Login from '../mains/Login'

const LoggedOutNavigator = createStackNavigator({
	Login:{
		screen:Login
	}
})

export default LoggedOutNavigator