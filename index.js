/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import AppNavRoot from './components/AppNavRoot';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => AppNavRoot);
