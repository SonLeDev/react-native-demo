import React, {Component} from 'react';
import { View, StyleSheet, Button ,ActivityIndicator} from 'react-native'
import { createAppContainer } from 'react-navigation'

import {getRootNavigator} from './navigator'
import { isLoggedIn } from './around25api/auth'


export default class AppNavRoot extends Component {

	constructor(props) {
	    super(props);
	    console.disableYellowBox = true;

	    this.state = {
	      loading: true,
	      loggedIn: false
    	};
  	}

   componentDidMount() {
    	const loggedIn =  isLoggedIn();
    	this.setState({ loggedIn, loading: false });
  	}

  	render() {
  		// show loading icon
    	if (this.state.loading) {
	      return (
	        <View style={styles.base}>
	          <ActivityIndicator size='small' />
	        </View>
	      )
	    }
	    const RootStack = getRootNavigator(this.state.loggedIn);

	    // createAppContainer required from ^V3 of react-navigation
	    const RootNavigatorApp = createAppContainer(RootStack);

	    return <RootNavigatorApp />
  	}
}

const styles = StyleSheet.create({
  base: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
